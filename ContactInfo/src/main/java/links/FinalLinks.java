package links;

public class FinalLinks {

	// 1. Code to keep/ eliminate domain specific social links.
	// 2. If flag true then keep social links according to domain specified
	// 3. If flag false then remove social links according to domain specified.
	/**
	 * <h1>Valid Links Common Code</h1>
	 * 
	 * <b>Parameters : </b>
	 * <ul>
	 * <li> boolean flag : true or false.
	 * <li> String domainName : domain name is given here. Or Output of Main Domain Extraction common code.
	 * <li> String links : Multiple or single list of string values seperated by comma. Most probable all social links or email id's.
	 * </ul>
	 * <p>Code to keep/ eliminate domain specific social links and email id's.<P>
	 * 
	 * <b>Return :</b><br>
	 * <ul>
	 * <li> If flag true then keep social links and email id's according to domain specified seperated by comma.
	 * <li> If flag false then remove social links and email id's according to domain specified seperated by comma.
	 * </ul>
	 */
	public String ValidLinks(boolean flag, String domain, String links) {

		domain = domain.replaceAll("<[^>]*?>", "").replaceAll("[^0-9a-zA-Z\n ]", "").trim();
		String[] allLinks = links.split(",");
		String newSocialLink = " ";
		// flag = true so keep social links according to domain specified
		for (String link : allLinks) {
			String oldLink = link;
			link = link.replaceAll("<[^>]*?>", "").replaceAll("[^0-9a-zA-Z\n ]", "").trim();
			if (flag == true) {
				if (link.contains(domain)) {
					newSocialLink = newSocialLink + "," + oldLink;
				}
			} else {
				if (!link.contains(domain)) {
					newSocialLink = newSocialLink + "," + oldLink;
				}
			}
		}
		
		newSocialLink = newSocialLink.trim();
		if (newSocialLink.startsWith(","))
			newSocialLink = newSocialLink.substring(1);
		newSocialLink = newSocialLink.trim();
		return newSocialLink;
	}
}
