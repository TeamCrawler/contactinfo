package phoneNumber;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtility 
{	
	private static String synonyms = "(([^a-zA-Z])?("
			+ "(?<!(Port|application|model|transaction|part|BID|serial|INN|licence|order|page) *)Number|"
			+ "Tel( +Num(ber)?)?( +no)?( +is)?|contact(s)?( +(us|me))?|Switchboard|Phone( +Num(ber)?)?( +no)?( +is)?|Fax( +Num(ber)?)?|"
			+ "Company +(Num(ber)?|no)|contact( +Num(ber)?)?( +no)?( +is)?|Sales|Support|Toll( +|^\\w)Free|CALL(s)?( +TODAY|to)?( +ON)?|"
			+ "call( +us( +for)?( +today)?( +free)?( +directly)?|( +sales)? +at)?( +\\D+|today)?|"
			+ "Main|Telephone|Телефон|Факс|факс|(land|hot|fixed +)line|cell?|Telegram|Viber|PLEASE( *DIAL( *AT)?)?|"
			+ "talk|Kontakty|phn|Whats *App( +Num(ber)?)?( +no)?|mobile|For more information|General|"
			+ "Local|[^0-9]\\+( +)?[0-9]+|(\\(?[0-9]+ +)?\\([0-9]{2,3}\\)( +)?[0-9]|"
			+ "Japan|Europe|India|USA|China|ABN)";

	public static Pattern removeDotsFromNo = Pattern.compile("[0-9] *\\. *[0-9]");
	public static Pattern separateNumber = Pattern.compile("[^a-zA-Z:]*[0-9]+( +)?\\/( +)?[^a-zA-Z]+");

	public static Pattern PhonePattern = Pattern
			.compile(synonyms+"[^a-zA-Z]*[0-9]+[^a-zA-Z]*(or *[^a-zA-Z]*)?)",Pattern.CASE_INSENSITIVE);
	//|[0-9]{3}( |-|\\.)[0-9]{3}( |-|\\.)[0-9]{3,4}
	public static void main(String[] args) 
	{		
		String textToProcess = "Tel:0086-577-61718113 number:0086-18367862388 are below or also can be as your required Mode";
		System.out.println(textToProcess);
		Matcher matcher = RegexUtility.PhonePattern.matcher(textToProcess);
		System.out.println("Matcher OutPut : ");
		while(matcher.find())
		{
			System.out.println("==>"+matcher.group());
		}		

	}
}
