package phoneNumber;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.regex.Matcher;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.google.i18n.phonenumbers.PhoneNumberMatch;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.Leniency;
import com.kohlschutter.boilerpipe.BoilerpipeProcessingException;
import com.kohlschutter.boilerpipe.extractors.KeepEverythingExtractor;



public class GetPhoneNumbers  
{
	static LinkedHashSet<String> country = new countryList().getCountry();

	public static void main(String[] args) throws IOException, BoilerpipeProcessingException 
	{

		Document doc = Jsoup.connect("http://www.translationdirectory.com/job_00034207.php")
				.userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.120 Safari/535.2")
				.referrer("http://www.google.com") 
				.header("Accept-Language", "en")
				.timeout(180000)
				.followRedirects(true)
				.get();
		String textToProcess = KeepEverythingExtractor.INSTANCE.getText(doc.toString());
		textToProcess = StringUtils.stripAccents(textToProcess);
		System.out.println("\nFinal O/P :"+new GetPhoneNumbers().getPhone(textToProcess));
	}

	/**
	 * <h1>Phone Finder Common Code</h1>
	 * 
	 * <b>Parameters : </b>
	 * <ul>
	 * <li> textProcess :String parameter on which we want to process.
	 * </ul>
	 * <p> Regex is applied on given parameter value and set of all Phone numbers are returned which 
	 * are seperated by comma(,). <P>
	 * 
	 * Method will covers following scenarious<br><br>
	 * <ul>
	 * <li> Extracts numbers followed by Synonym 
	 * <li> Numbers starts with +
	 * <li> Number containing numbers in brackets and bracket contains (2 or 3 ) numbers.
	 * <li>  US/Uk number pattern. Number in 3-3-3/4 blocks.
	 *  (Ex 123 123 1234 / 123-123-1234 / 123.123.1234 || 123 123 123 / 123-123-123 / 123.123.123)
	 * </ul>
	 * <b>Return :</b><br>
	 * String is returned which contains all Phone numbers seperated by comma (,)
	 * 
	 */
	public String getPhone(String text)
	{
		HashMap<String,String> numMap = new HashMap<String,String>();
		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		PhoneNumberMatch match;
		String presentNum="",PlainPresentNum="";

		String textToProcess = ApplyPhoneRegex(text).replaceAll("[\\[\\]]", "");
		//System.out.println("Regex o/p : "+textToProcess);
		if(textToProcess.equals("null"))
			return "";

		textToProcess = separateNumbers(textToProcess);

		Iterator<String> countryItr = country.iterator();


		while(countryItr.hasNext())
		{
			Iterator<PhoneNumberMatch> iterator = phoneUtil.findNumbers
					(textToProcess,countryItr.next(),Leniency.VALID, Long.MAX_VALUE).iterator();

			while(iterator.hasNext()) 
			{
				match = iterator.next();

				presentNum = match.rawString();//.replaceAll("^[^\\w]+", "");

				PlainPresentNum = presentNum.replaceAll("[^0-9]", "");
				if(PlainPresentNum.length()<10||PlainPresentNum.length()>15)
					continue;
				for (String currMapNum : numMap.keySet())
				{
					if(currMapNum.endsWith(PlainPresentNum))
					{
						presentNum = numMap.get(currMapNum);
						PlainPresentNum = presentNum.replaceAll("[^0-9]", "");
						continue;
					}
					if(PlainPresentNum.endsWith(currMapNum))
					{
						numMap.remove(currMapNum);
						break;
					}
				}

				numMap.put(PlainPresentNum,presentNum);
			}
		}
		return numMap.values().toString().replaceAll("[ +]", " ").replaceAll("[\\[\\]]", "").replaceAll("[\\.-]", " ").replaceAll(" +"," ");
	}

	/**
	 * This function extarcts the phone numbers from text by applying Regex stored in RegexUtility.
	 *  It extracts numbers followed by Synonym words and matching 3 custom logic.
	 *  1. Numbers starts with +
	 *  2. Number containing numbers in brackets and bracket contains (2 or 3 ) numbers.
	 *  3. US/Uk number pattern. Number in 3-3-3/4 blocks.
	 *  (Ex 123 123 1234 / 123-123-1234 / 123.123.1234 || 123 123 123 / 123-123-123 / 123.123.123)
	 * @param textToProcess
	 * @return
	 */
	private static String ApplyPhoneRegex(String textToProcess)
	{
		StringBuilder strBuilder=new StringBuilder("");
		StringBuilder builder;
		Matcher matcher = RegexUtility.PhonePattern.matcher(textToProcess);
		Matcher matcher1;
		int i = 1; 
		while(matcher.find())
		{

			builder = new StringBuilder(matcher.group());
			matcher1 = RegexUtility.removeDotsFromNo.matcher(builder.toString());
			i = 1;
			while(matcher1.find())
			{				
				//System.out.println(matcher1.start());
				builder.deleteCharAt(matcher1.start()+i--);
			}

			strBuilder.append("Vish=> "+builder.toString());
		}

		if(strBuilder.toString().equals(""))
			return "null";
		return strBuilder.toString().trim();
	}	
	/**
	 * Function created to handle +44(0)1306 740363/83 this kind of situation where 63 nad 83 are or number sets.
	 * In this case  +44(0)1306 740363 and +44(0)1306 740383 this two numbers are present.
	 * So this function adds +44(0)1306 740383 this nnumber at last of text by generating from number.
	 * @param textToProcess
	 * @return
	 */
	//Will explain function whith this ex : +44 (0) 1306 740363 / 83
	private static String separateNumbers(String textToProcess)
	{
		String tempMatcherStr="",newNum="",mainNum="";
		int len,i;
		//will matches the numbers from text where / presents;
		Matcher matcher = RegexUtility.separateNumber.matcher(textToProcess);

		while(matcher.find())
		{
			//tempMatcherStr =  +44 (0) 1306 740363 / 83
			tempMatcherStr = matcher.group();
			i = 0;
			len=0;
			for(String replacement : tempMatcherStr.split("/"))
			{
				replacement = replacement.replaceAll("[^0-9() ]", "").trim();
				if(i == 0)
				{
					//first number is base/main number 
					//so will set mainNum = +44 (0) 1306 740363
					i++;
					mainNum = replacement;
					continue;
				}
				//second iteration replacement = 83
				//len = 2
				len = replacement.trim().length();
				//Last numbers anre replaced by new replacement numbers, So new num will become
				//newNum = +44 (0) 1306 740383
				//replaces 63 by 83
				if(len <= mainNum.length())
					newNum = mainNum.substring(0,mainNum.length()-len)+replacement;
				else
					newNum = replacement;
				//adding newNum as new number in text
				textToProcess = textToProcess+" , separateNumber=>"+newNum;
			}
		}

		return textToProcess;
	}
}