package socialLinks;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Connection.Response;
import org.jsoup.HttpStatusException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.RedirectTest.RedirectTest;
import com.bean.ResponseBean;

import bean.SocialLinksBean;
import contactValues.ContactInfo;
import jsoupCode.JsoupDocument;

public class GetSocialLinks implements ContactInfo {

	static Pattern pattern = Pattern.compile(extracttwitterLinkRegex);
	static Matcher matcher;

	SocialLinksBean beanObject;

	/**
	 * <h1>Social Link Common Code</h1>
	 * 
	 * <b>Parameters : </b>
	 * <ul>
	 * <li>htmlDocument : Jsoup Document class reference.
	 * <li>link : String url of which htmlDocument is passed.
	 * </ul>
	 * <p>
	 * Regex is applied on all href links and then using getLink(), values are
	 * set using Setter methods.
	 * <P>
	 * 
	 * <b>Return :</b><br>
	 * Object of bean class SocialLinksBean is returned. Using getter method of
	 * this class you will get all the required values.
	 * 
	 */
	public SocialLinksBean socialLinks(Document htmlDoc, String link) throws IOException {
		beanObject = new SocialLinksBean();

		Elements links;
		// Matches query (Regex)
		links = htmlDoc.select("a[abs:href~=" + fbRegex + "]");
		beanObject.setFacebookLink(getLink(links, fbRegex).trim());

		links = htmlDoc.select("a[abs:href~=" + linkedinRegex + "]");
		beanObject.setLinkedinLink(getLink(links, linkedinRegex).trim());

		links = htmlDoc.select("a[abs:href~=" + twitterRegex + "]");
		beanObject.setTwitterLink(validateTwitterLink(getLink(links, twitterRegex)));

		links = htmlDoc.select("a[abs:href~=" + googleRegex + "]");
		beanObject.setGoogleLink(getLink(links, googleRegex).trim());

		links = htmlDoc.select("a[abs:href~=" + instaRegex + "]");
		beanObject.setInstagramLink(getLink(links, instaRegex).trim());

		links = htmlDoc.select("a[abs:href~=" + pinterestRegex + "]");
		beanObject.setPintrestLink(getLink(links, pinterestRegex).trim());

		return beanObject;
	}

	public String validateTwitterLink(String link) throws IOException {
		Set<String> twitterLink = new HashSet<String>();
		matcher = pattern.matcher(link);

		while (matcher.find()) {
			try {
				Response res = new JsoupDocument().jsoupDocument(matcher.group());
				if (res.statusCode() == 200)
					twitterLink.add(matcher.group());
			} catch (Exception e) {
				System.out.println("Error for " + matcher.group());
			}
		}

		return twitterLink.toString().substring(1, twitterLink.toString().length() - 1);
	}

	// Get Social Links from Homepage according to regex provided
	public String getLink(Elements links, String regex) {
		String socialLink = " ";
		String dummySocialLink = " ";
		String mainLink;
		ResponseBean res;
		try {

			for (Element link : links) {

				String finalLink = "";
				try {
					finalLink = link.absUrl("abs:href").toLowerCase();
					finalLink = finalLink.replace("http://", "https://");
					if (finalLink.contains("?")) {
						finalLink = finalLink.substring(0, finalLink.indexOf("?"));
					}
				} catch (Exception e) {
					continue;
				}
				if (!(link.absUrl("abs:href").toLowerCase().contains("/share")
						|| link.absUrl("abs:href").toLowerCase().contains("twitter.com/twitter")
						|| link.absUrl("abs:href").toLowerCase().contains("twitter.com/intent")
						|| link.absUrl("abs:href").toLowerCase().contains("twitter.com/search")
						|| link.absUrl("abs:href").toLowerCase().contains("twitter.com/home")
						|| link.absUrl("abs:href").toLowerCase().contains("facebook.com/facebook")
						|| link.absUrl("abs:href").toLowerCase().contains("facebook.com/dialog")
						|| link.absUrl("abs:href").toLowerCase().contains("linkedin.com/linkedin")
						|| link.absUrl("abs:href").toLowerCase().contains("linkedin.com/groups")
						|| link.absUrl("abs:href").toLowerCase().contains("pinterest.com/pinterest")
						|| link.absUrl("abs:href").toLowerCase().contains("pinterest.com/pin")
						|| link.absUrl("abs:href").toLowerCase().contains("google.com/+google")
						|| link.absUrl("abs:href").toLowerCase().contains("google.com/+gmail"))
						&& Pattern.matches(regex, finalLink)) {

					/**
					 * Commented code which hitting Social links.
					 * This is custom logiic for Apps Module
					 */
					/*res = RedirectTest.checkRedirectingURL(finalLink, "social");
					System.out.println(res.status + " for " + finalLink);
					if (!res.status)
						continue;*/

					if (!socialLink.contains(finalLink)) {
						mainLink = finalLink.substring(finalLink.indexOf(getDomainName(finalLink)));
						if (!dummySocialLink.contains(mainLink)) {
							dummySocialLink = dummySocialLink + "," + mainLink;
							socialLink = socialLink + "," + finalLink;
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		socialLink = socialLink.trim();
		if (socialLink.startsWith(","))
			socialLink = socialLink.substring(1);
		return socialLink;
	}

	public static void main(String[] args) throws MalformedURLException {
		String str = "http://twitter.com/blumeventures";
		String str1 = "https://twitter.com/blumeventures";
		String str2 = "https://www.twitter.com/blumeventures";
		String str3 = "http://www.twitter.com/blumeventures";

		/*
		 * GetSocialLinks obj = new GetSocialLinks(); try {
		 * System.out.println(str.substring(str.indexOf(obj.getDomainName(str)))
		 * );
		 * System.out.println(str1.substring(str1.indexOf(obj.getDomainName(str1
		 * ))));
		 * System.out.println(str2.substring(str2.indexOf(obj.getDomainName(str2
		 * ))));
		 * System.out.println(str3.substring(str3.indexOf(obj.getDomainName(str3
		 * )))); } catch (URISyntaxException e) { // TODO Auto-generated catch
		 * block e.printStackTrace(); }
		 */
		String str4 = str1.replace("http://", "https://");
		if (!str4.contains("www"))
			str4 = str4.replace("https://", "https://www.");
		System.out.println(str4);
	}

	public String getDomainName(String url) throws URISyntaxException {
		URI uri = new URI(url);
		String domain = uri.getHost();
		return domain.startsWith("www.") ? domain.substring(4) : domain;
	}
}
