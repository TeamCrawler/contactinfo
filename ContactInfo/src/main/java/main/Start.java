package main;

import java.io.IOException;

import org.jsoup.nodes.Document;

import com.kohlschutter.boilerpipe.extractors.KeepEverythingExtractor;
import com.mongodb.BasicDBObject;
import com.mongodb.Bytes;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import bean.SocialLinksBean;
import emailLinks.GetEmailLinks;
import getDomain.DomainExtraction;
import jsoupCode.JsoupDocument;
import links.FinalLinks;
import phoneNumber.GetPhoneNumbers;
import socialLinks.GetSocialLinks;

public class Start {

	public static void main(String[] args) throws IOException {

		MongoClient mongoClient = new MongoClient("localhost", 27017);

		// Now connect to your databases
		DB db = mongoClient.getDB("crawler");
		DBCollection InvestorsTesting = db.getCollection("sampleCrawldata");
		DBCollection ContactInfo = db.getCollection("ContactInfo");
		
		Document doc;
		GetSocialLinks socialLinks = new GetSocialLinks();
		SocialLinksBean socialLinksBean;
		String link, mainContent;
		GetEmailLinks emailLinks = new GetEmailLinks();
		GetPhoneNumbers phoneFinder = new GetPhoneNumbers();
		DBCursor cur = InvestorsTesting.find();
		cur.addOption(Bytes.QUERYOPTION_NOTIMEOUT);
		DBObject object;
		BasicDBObject obj = new BasicDBObject();
		while (cur.hasNext()) {
			try {
				object = cur.next();
				link = (String) object.get("link");
				System.out.println(link);
				doc = new JsoupDocument().jsoupDocument(link).parse();
				mainContent = KeepEverythingExtractor.INSTANCE.getText(doc.toString()).replaceAll("<[^>]*?>", "");
				socialLinksBean = socialLinks.socialLinks(doc, link);
				
				obj.put("link", link);
				String domain = DomainExtraction.getDomainName(link);
				String facebook = new FinalLinks().ValidLinks(false,domain , socialLinksBean.getFacebookLink());
				String twitter =  new FinalLinks().ValidLinks(false, domain, socialLinksBean.getTwitterLink());
				String linkedin =  new FinalLinks().ValidLinks(false, domain, socialLinksBean.getLinkedinLink());
				String google =  new FinalLinks().ValidLinks(false, domain, socialLinksBean.getGoogleLink());
				String insta =  new FinalLinks().ValidLinks(false, domain, socialLinksBean.getInstagramLink());
				String pin =  new FinalLinks().ValidLinks(false, domain, socialLinksBean.getPintrestLink());
				String email = new FinalLinks().ValidLinks(false, domain, emailLinks.getEmailId(mainContent));
				String emailId = new FinalLinks().ValidLinks(false, domain, emailLinks.getEmailIdMailTo(doc));
				obj.put("facebookLink", facebook);
				obj.put("twitterLink", twitter);
				obj.put("LinkedinLink", linkedin);
				obj.put("googleLink", google);
				obj.put("instagramLink", insta);
				obj.put("pinterestLink", pin);
				obj.put("email", email);
				obj.put("mailTo", emailId);
				obj.put("phoneNumber",phoneFinder.getPhone(mainContent));
				ContactInfo.insert(obj);
				obj.clear();
				
				System.out.println("Facebook Link " + facebook);
				System.out.println("Linkedin Link " + linkedin);
				System.out.println("Twitter Link " + twitter);
				System.out.println("Google Link " + google);
				System.out.println("Instagram Link " + insta);
				System.out.println("Pinterest Link " + pin);
				System.out.print("Email :- ");
				System.out.println(email);
				System.out.print("Email Mail to :- ");
				System.out.println(emailId);
				System.out.print("Phone Number :- ");
				System.out.println(phoneFinder.getPhone(mainContent));
				System.out.println("--------------------------------------------");
			} catch (Exception e) {
				continue;
			}
		}
	}
}
