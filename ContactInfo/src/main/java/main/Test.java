package main;

import java.io.IOException;
import java.util.LinkedHashSet;

import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.kohlschutter.boilerpipe.BoilerpipeProcessingException;
import com.kohlschutter.boilerpipe.extractors.KeepEverythingExtractor;

import bean.SocialLinksBean;

import emailLinks.GetEmailLinks;
import getDomain.DomainExtraction;
import jsoupCode.JsoupDocument;
import links.FinalLinks;
import phoneNumber.GetPhoneNumbers;
import socialLinks.GetSocialLinks;

public class Test {

	public static void main(String[] args) throws IOException, BoilerpipeProcessingException {

		GetSocialLinks socialLinks = new GetSocialLinks();
		GetEmailLinks emailLinks = new GetEmailLinks();
		GetPhoneNumbers phoneFinder = new GetPhoneNumbers();
		// String link = args[0];

		String link = "https://www.dice.com/jobs/detail/Sr.-Java-Developer-%28Heavy-telecommute-option%29-Confidential-Company-Culver-City-CA-90230/10356951/485022?icid=sr30-1p&q=telecommute&l=";
		// String link =
		// "https://www.stage32.com/webinars/How-To-Develop-Your-Narrative-Voice-As-An-Editor";
		Response response = new JsoupDocument().jsoupDocument(link);
		Document doc = response.parse();
		
		String mainContent = KeepEverythingExtractor.INSTANCE.getText(doc.toString()).replaceAll("<[^>]*?>", "");
		SocialLinksBean socialLinksBean = socialLinks.socialLinks(doc, link);

		System.out.println("---------------------------------------------");
		System.out.println("Facebook Link " + socialLinksBean.getFacebookLink());
		System.out.println("Linkedin Link " + socialLinksBean.getLinkedinLink());
		System.out.println("Twitter Link " + socialLinksBean.getTwitterLink());
		System.out.println("Google Link " + socialLinksBean.getGoogleLink());
		System.out.println("Instagram Link " + socialLinksBean.getInstagramLink());
		System.out.println("Pinterest Link " + socialLinksBean.getPintrestLink());
		System.out.print("Email :- ");
		System.out.println(emailLinks.getEmailId(mainContent));
		System.out.print("Email Mail to :- ");
		System.out.println(emailLinks.getEmailIdMailTo(doc));
		System.out.print("Phone Number :- ");
		System.out.println(phoneFinder.getPhone(mainContent));

		System.out.println("----------------------------------------------");
		System.out.println("Links for which domain name matches");
		String domain = DomainExtraction.getDomainName(link);
		System.out.println(
				"Facebook Link " + new FinalLinks().ValidLinks(true, domain, socialLinksBean.getFacebookLink()));
		System.out.println(
				"Linkedin Link " + new FinalLinks().ValidLinks(true, domain, socialLinksBean.getLinkedinLink()));
		System.out
				.println("Twitter Link " + new FinalLinks().ValidLinks(true, domain, socialLinksBean.getTwitterLink()));
		System.out.println("Google Link " + new FinalLinks().ValidLinks(true, domain, socialLinksBean.getGoogleLink()));
		System.out.println(
				"Instagram Link " + new FinalLinks().ValidLinks(true, domain, socialLinksBean.getInstagramLink()));
		System.out.println(
				"Pinterest Link " + new FinalLinks().ValidLinks(true, domain, socialLinksBean.getPintrestLink()));
		System.out.print("Email :- ");
		System.out.println(new FinalLinks().ValidLinks(true, domain, emailLinks.getEmailId(mainContent)));
		System.out.print("Email Mail to :- ");
		System.out.println(new FinalLinks().ValidLinks(true, domain, emailLinks.getEmailIdMailTo(doc)));

		System.out.println("----------------------------------------------");
		System.out.println("Links for which domain name does not matches");
		System.out.println(
				"Facebook Link " + new FinalLinks().ValidLinks(false, domain, socialLinksBean.getFacebookLink()));
		System.out.println(
				"Linkedin Link " + new FinalLinks().ValidLinks(false, domain, socialLinksBean.getLinkedinLink()));
		System.out.println(
				"Twitter Link " + new FinalLinks().ValidLinks(false, domain, socialLinksBean.getTwitterLink()));
		System.out
				.println("Google Link " + new FinalLinks().ValidLinks(false, domain, socialLinksBean.getGoogleLink()));
		System.out.println(
				"Instagram Link " + new FinalLinks().ValidLinks(false, domain, socialLinksBean.getInstagramLink()));
		System.out.println(
				"Pinterest Link " + new FinalLinks().ValidLinks(false, domain, socialLinksBean.getPintrestLink()));
		System.out.print("Email :- ");
		System.out.println(new FinalLinks().ValidLinks(false, domain, emailLinks.getEmailId(mainContent)));
		System.out.print("Email Mail to :- ");
		System.out.println(new FinalLinks().ValidLinks(false, domain, emailLinks.getEmailIdMailTo(doc)));
	}
}
