package contactValues;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public interface ContactInfo {
	final static String fbRegex = "((http|https)://)?(www[.])?(in.)?facebook.com/.+";
	final static String linkedinRegex = "((http|https)://)?(www[.])?(in.)?linkedin.com/.+";
	final static String twitterRegex = "((http|https)://)?(www[.])?(in.)?twitter.com/.+";
	final static String extracttwitterLinkRegex = "((http|https)://)?(www[.])?twitter.com\\/[\\w-_@]+";
	final static String googleRegex = "((http|https)://)?(www[.])?plus.google.com/.+";
	final static String instaRegex = "((http|https)://)?(www[.])?(in[.])?instagram.com/.+";
	final static String pinterestRegex = "((http|https)://)?(www[.])?(in.)?pinterest.com/.+";
	
	final static String openingBracketRegex = "(\\[|\\(|\\{|\\<)";
	final static String closingBracketRegex = "(\\]|\\)|\\}|\\>)";
	final static String domainRegex = "[A-Za-z0-9\\-]{2,} ?";
	//final static String domainRegex = "\\w*\\D{1,}\\w*";
	final static String dotRegex = "(\\.| dot )";
	final static String afterDotRegex = "([a-zA-Z\\-]{2,}\\.?)+ ?";
	final static String nameRegex = "[A-Za-z0-9\\_\\.\\-]*[A-Za-z]+[A-Za-z0-9\\_\\.\\-]* {0,2}";
	
	public static void main(String[] args) {
		
		String str = "Experience working in a structured healthcare environment is a huge plus.<br /> <br /> <br /><br /> <br />Thanks and Regards…..<br /> <br />Alex<br />Norwin Technologies<br />Talent Acquisition Group <br />Phone: 978 - 707 - 4848<br />Alex@norwintechnologies.com<br />www.norwintechnologies.com<br />Description: cid:1.2174102670@web113604.mail.gq1.yahoo.com";
		Matcher m = Pattern.compile(nameRegex + "(@|" + openingBracketRegex + "at" + closingBracketRegex + ") *" + domainRegex
				+ "+" + dotRegex + afterDotRegex).matcher(str);
		while (m.find()) 
		{
			System.out.println("**"+m.group());
		}
	}
}
