package emailLinks;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.kohlschutter.boilerpipe.BoilerpipeProcessingException;
import com.kohlschutter.boilerpipe.extractors.KeepEverythingExtractor;

import contactValues.ContactInfo;

import jsoupCode.JsoupDocument;
import socialLinkTest.EmailReject;

/**
 * 
 * @author AJ<br>
 * SourcePep Pvt Lmtd.<br>
 * Hadapsar , Pune , 411028<br>
 * <b>All rights reserved.</b>
 */
public class GetEmailLinks implements ContactInfo {

	LinkedHashSet<String> emailID;

	public static void main(String[] args) throws IOException, BoilerpipeProcessingException {

/*		Document doc = new JsoupDocument()
				.jsoupDocument("http://www.leadlake.com").parse();
		String mainContent = KeepEverythingExtractor.INSTANCE.getText(doc.toString()).replaceAll("<[^>]*?>", "");*/
		System.out.println(new GetEmailLinks().getEmailId("work@home..... odd.ajit@gmail.co.in "));
	}

	/**
	 * <h1>Email ID Common Code</h1>
	 * 
	 * <b>Parameters : </b>
	 * <ul>
	 * <li> textProcess :String parameter on which we want to process.
	 * </ul>
	 * <p> Regex is applied on given parameter value and set of all email id's are returned which 
	 * are seperated by comma(,). <P>
	 * 
	 * Method will covers following scenarious<br><br>
	 * <ul>
	 * <li> fetch normal email id's like someone@something.com
	 * <li> fetch normal email id's like someone @something.com
	 * <li> fetch normal email id's like someone at something.com
	 * <li> fetch normal email id's like someone at something dot com
	 * </ul>
	 * <b>Return :</b><br>
	 * String is returned which contains all email id's seperated by comma (,)
	 * 
	 */
	public String getEmailId(String textProcess) {
		textProcess = " " + textProcess + " ";
		emailID = new LinkedHashSet<String>();
		Matcher m;
		// Email Id regex.

		// m = Pattern.compile("[\\w_\\.+-]+ *(@|(\\[|\\()at(\\]|\\)))
		// +[\\w-]+\\.[\\w-\\.]{2,5} ").matcher(textProcess);
		m = Pattern.compile(nameRegex + "(@|" + openingBracketRegex + "at" + closingBracketRegex + ") *" + domainRegex
				+ "+" + dotRegex + afterDotRegex).matcher(textProcess);
		String temp1;
		while (m.find()) {
			
			temp1 = m.group().toLowerCase().trim().replaceAll("\\.+", ".");
			
			if (temp1.startsWith(".")) {
				temp1 = temp1.substring(1, temp1.length());
			}

			if (temp1.endsWith(".")) {
				temp1 = temp1.substring(0, temp1.length() - 1);
			}

			temp1 = temp1.replaceAll((openingBracketRegex + "at" + closingBracketRegex), "@");
			temp1 = temp1.replaceAll("\\sdot\\s", ".");
			temp1 = temp1.replaceAll(" +", "");
			if(new EmailReject().TestLink(temp1))
			{
				emailID.add(temp1);
			}
		}

		return emailID.toString().replaceAll("[\\[\\]]", "");
	}

	/**
	 * <h1>Mail To Email ID Common Code</h1>
	 * 
	 * <b>Parameters : </b>
	 * <ul>
	 * <li> htmlDocument : Jsoup Document class reference. 
	 * </ul>
	 * <p> Regex is applied on all href links which matches 'mailto' regex and values are returned which 
	 * are seperated by comma(,). <P>
	 * 
	 * <b>Return :</b><br>
	 * String is returned which contains all email id's seperated by comma (,)
	 * 
	 */
	public String getEmailIdMailTo(Document doc) {
		emailID = new LinkedHashSet<String>();
		Matcher m;
		// Email Id regex.

		Elements elements = doc.select("a[href~= *mailto]");
		for (Element element : elements) {
			m = Pattern.compile("[a-zA-Z0-9_.+-]+ *@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+").matcher(element.attr("href"));
			String temp1;
			while (m.find()) {
				// System.out.println("In Mailto="+element.attr("href")+" =>
				// "+m.group());
				temp1 = m.group().toLowerCase().trim().replaceAll("\\.+", ".");

				if (temp1.startsWith(".")) {
					temp1 = temp1.substring(1, temp1.length());
				}

				if (temp1.endsWith(".")) {
					temp1 = temp1.substring(0, temp1.length() - 1);
				}
				if(new EmailReject().TestLink(temp1))
				{
				emailID.add(temp1);
				}
			}
		}
		return emailID.toString().replaceAll("[\\[\\]]", "");
	}
}
