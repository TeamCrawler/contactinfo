package socialLinkTest;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import getDomain.DomainExtraction;

/**
 * Author : AJ
 * SourcePep Technology.
 * Hadapsar , Pune , 411028
 * All rights reserved.
 */
public class EmailReject {

	/**
	 * <h1> :	Link Testing : </h1>
	 * 
	 * <b>Parameters : </b>
	 * <ul>
	 * <li> url : any url of type String
	 * </ul>
	 * <p> This module is designed for checking wether link is social link or not. <P>
	 * 
	 * <b>Return :</b><br>
	 * Status false if and only if above url is  Social link.<br>
	 * else true;
	 * 
	 */
	static Set<String> link = new HashSet<>();
	static{
		BufferedReader br = null;
		FileReader fr = null;
		try {
			fr = new FileReader("./emailReject.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		br = new BufferedReader(fr);
		try {
			br = new BufferedReader(new FileReader("./emailReject.txt"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String sCurrentLine;
		try {
			while ((sCurrentLine = br.readLine()) != null) {
				link.add(sCurrentLine.trim().toLowerCase());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean TestLink(String url) {
		return !link.contains(url.toLowerCase());
	}
	
	public static void main(String[] args) {
		System.out.println(new EmailReject().TestLink("http://sourcepep.com"));
	}
}
